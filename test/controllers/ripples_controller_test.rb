require 'test_helper'

class RipplesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ripple = ripples(:one)
    @ripples = ripples
  end

  test "should get index" do
    get ripples_url
    assert_response :success
  end

  test "should get new" do
    get new_ripple_url
    assert_response :success
  end

  test "should create ripple" do
    assert_difference('Ripple.count') do
      post ripples_url, params: { ripple: { message: @ripple.message, name: @ripple.name, url: @ripple.url } }
    end

    assert_redirected_to ripple_url
  end

  test "should show ripple" do
    get ripple_url(@ripple)
    assert_response :success
  end

  test "should move to next page" do
    get ripples_path
    assert_equal 0, session[:page]
 
    get next_ripples_path
    assert_equal 1, session[:page]

    get ripples_path
    assert_equal 0, session[:page]
  end

  test "should move to previous page" do
    get oldest_ripples_path
    assert_equal 2, session[:page]

    get prev_ripples_path
    assert_equal 1, session[:page]
  end


end
