# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Ripple.delete_all

require 'csv'

csv_text = File.read(Rails.root.join('lib', 'seeds', 'Ripples_Data.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')

csv.each do |r|
  ripple = Ripple.new
  ripple.name = r['name']
  ripple.url = r['url']
  ripple.message = r['message']
  ripple.save
end

puts "#{Ripple.count} rows in Ripples table"
