Rails.application.routes.draw do
  resources :ripples, :except => [:edit, :destroy, :update] do
    collection do
      match 'next', :via => [:get]
      match 'prev', :via => [:get]
      match 'oldest', :via => [:get]
    end
  end


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
