class Ripple < ApplicationRecord
  validates :name, :message, presence: true
  validates :url, allow_blank: true, format: { with: URI::regexp(%w(http https)), message: "URL must begin with http:// or https://" }
end
