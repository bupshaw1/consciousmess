class RipplesController < ApplicationController
  before_action :set_ripple, only: [:show]
  PER_PAGE = 10

  # GET /ripples
  # GET /ripples.json
  def index
    session[:page] = 0
    @ripples = Ripple.all.order("created_at DESC").limit(PER_PAGE).offset(session[:page] * PER_PAGE)
    @total = Ripple.count / PER_PAGE
    if(0..@total).include?(@page.to_i) == FALSE
      redirect_to ripples_path
    end
  end

  # GET /ripples/1
  # GET /ripples/1.json
  def show
  end

  # GET /ripples/new
  def new
    @ripple = Ripple.new
  end

  # GET /ripples/1/edit
#  def edit
#  end

  # POST /ripples
  # POST /ripples.json
  def create
    @ripple = Ripple.new(ripple_params)

    respond_to do |format|
      if @ripple.save
        format.html { redirect_to ripples_path, notice: 'Ripple was successfully created.' }
        format.json { render :show, status: :created, location: @ripple }
      else
        format.html { render :new }
        format.json { render json: @ripple.errors, status: :unprocessable_entity }
      end
    end
  end

  def next
    if session[:page] != Ripple.count / PER_PAGE
      session[:page] += 1
      @ripples = Ripple.all.order("created_at DESC").limit(PER_PAGE).offset(session[:page] * PER_PAGE)
      render 'ripples/index'
    else
      redirect_to ripples_path
    end
  end

  def prev
    if session[:page] != 0
      session[:page] -= 1
      @ripples = Ripple.all.order("created_at DESC").limit(PER_PAGE).offset(session[:page] * PER_PAGE)
      render 'ripples/index'
    else
      redirect_to ripples_path
    end
  end

  def oldest
    session[:page] = Ripple.count / PER_PAGE
      @ripples = Ripple.all.order("created_at DESC").limit(PER_PAGE).offset(session[:page] * PER_PAGE)
      render 'ripples/index'
  end

  # PATCH/PUT /ripples/1
  # PATCH/PUT /ripples/1.json
#  def update
#    respond_to do |format|
#      if @ripple.update(ripple_params)
#        format.html { redirect_to @ripple, notice: 'Ripple was successfully updated.' }
#        format.json { render :show, status: :ok, location: @ripple }
#      else
#        format.html { render :edit }
#        format.json { render json: @ripple.errors, status: :unprocessable_entity }
#      end
#    end
#  end

  # DELETE /ripples/1
  # DELETE /ripples/1.json
#  def destroy
#    @ripple.destroy
#    respond_to do |format|
#      format.html { redirect_to ripples_url, notice: 'Ripple was successfully destroyed.' }
#      format.json { head :no_content }
#    end
#  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ripple
      @ripple = Ripple.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ripple_params
      params.require(:ripple).permit(:name, :url, :message)
    end
  end

